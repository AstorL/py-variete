import unittest
import database, database_structure
from datetime import datetime,time
from hashlib import sha512


class MyTestCase(unittest.TestCase):
    def setUp(self):
        self.acc_base = database.AccountFactory() 
        self.ampluah_base = database.AmpluahFactory() 
        self.occ_base = database.OccupationFactory() 
        self.posters_base = database.PosterFactory() 
        self.recvisite_base = database.RecvisiteFactory() 
        self.repetition_base = database.RepetitionFactory() 
        self.rent_base = database.RentFactory() 
        self.role_base = database.RoleFactory() 
        self.shows_base = database.ShowsFactory() 
        self.tittle_base = database.TittleFactory()
        self.type_base = database.TypeFactory()

    def test_add_account_entry(self):
        test_fio = "Unittest Account"
        test_login = 'U-Test Acc'
        test_password = sha512('test'.encode('utf-8')).hexdigest()
        test_address = 'UnittestCore'
        test_sex = 'женский'
        test_birthday = datetime(1998, 11, 23)
        self.acc_base.add_entry(test_fio, test_login, test_password,test_address,test_sex,test_birthday)
        a = len(self.acc_base.accounts)
        acc_id = 0
        self.assertEqual(test_fio, self.acc_base.accounts[a-1].fio.value)
        self.assertEqual(test_login, self.acc_base.accounts[a-1].login.value)
        self.assertEqual(test_password, self.acc_base.accounts[a-1].password.value)
        self.assertEqual(test_address, self.acc_base.accounts[a-1].address.value)
        self.assertEqual(test_sex, self.acc_base.accounts[a-1].sex.value)
        self.assertEqual(test_birthday, self.acc_base.accounts[a-1].birthday.value)

    def test_delete_account_entry(self):
        a = len(self.acc_base.accounts)
        self.acc_base.delete_entry(self.acc_base.accounts[a-1].id.value)
        self.assertEqual(a-1, len(self.acc_base.accounts))

    def test_add_ampluah_entry(self):
        test_name = "test ampluah"
        self.ampluah_base.add_entry(test_name)
        a = len(self.ampluah_base.ampluah_list)
        self.assertEqual(test_name, self.ampluah_base.ampluah_list[a-1].name.value)

    def test_delete_ampluah_entry(self):
        a = len(self.ampluah_base.ampluah_list)
        self.acc_base.delete_entry(self.ampluah_base.ampluah_list[a-1].id.value)
        self.assertEqual(a, len(self.ampluah_base.ampluah_list))

    def test_add_occupation_entry(self):
        test_occupation = "U-Test Occupation"
        test_tittle1 = database_structure.Tittle()
        test_tittle1.id.value = 0
        test_tittle1.name.value = "U-Test tittle"
        test_tittles = [test_occupation]
        self.occ_base.add_entry(test_occupation, test_tittles)
        a = len(self.occ_base.occupations)
        self.assertEqual(test_occupation, self.occ_base.occupations[a-1].name.value)
        self.assertEqual(test_tittles, self.occ_base.occupations[a-1].tittles)

    def test_delete_occupation_entry(self):
        a = len(self.occ_base.occupations)
        self.occ_base.delete_entry(self.occ_base.occupations[a-1].id.value)
        self.assertEqual(a-1, len(self.occ_base.occupations))

    def test_add_poster_entry(self):
        a = 0
        test_show = database_structure.Show()
        test_show.id.value = 0
        test_show.name.value = "UnitTest Show"
        test_date = datetime(2020, 3, 5, 18, 50)
        test_duration = time(3, 50)
        self.posters_base.add_entry(test_show,test_date,test_duration)
        a = len(self.posters_base.posters)
        self.assertEqual(test_show,self.posters_base.posters[a-1].show)
        self.assertEqual(test_date,self.posters_base.posters[a-1].date.value)
        self.assertEqual(test_duration,self.posters_base.posters[a-1].duration.value)

    def delete_poster_entry(self):
        a = len(self.posters_base.posters)
        self.posters_base.delete_entry(self.posters_base.posters[a-1].id.value)
        self.assertEqual(a-1,len(self.posters_base.posters))

    def test_add_recvisite_entry(self):
        a = 0
        test_type = database_structure.RecvisiteType()
        test_type.id.value = 0
        test_type.type_name.value = "Unittest type"
        test_name = "Unittest recvisite"
        test_price = 420.00
        self.recvisite_base.add_entry(test_name,test_price,test_type)
        a = len(self.recvisite_base.recvisite_list)
        self.assertEqual(test_name, self.recvisite_base.recvisite_list[a-1].name.value)
        self.assertEqual(test_price, self.recvisite_base.recvisite_list[a-1].price.value)
        self.assertEqual(test_type, self.recvisite_base.recvisite_list[a-1].recv_type)

    def test_delete_recvisite_entry(self):
        a = len(self.recvisite_base.recvisite_list)
        self.recvisite_base.delete_entry(self.recvisite_base.recvisite_list[a-1].id.value)
        self.assertEqual(a-1,len(self.recvisite_base.recvisite_list))

    def test_add_repetition_entry(self):
        a = 0
        test_show = database_structure.Show()
        test_show.id.value = 0
        test_show.name.value = "UnitTest Show"
        test_date = datetime(2020, 3, 5, 18, 50)
        test_duration = time(3, 50)
        self.repetition_base.add_entry(test_date,test_duration,test_show)
        a = len(self.repetition_base.repetitions)
        self.assertEqual(test_date, self.repetition_base.repetitions[a-1].date.value)
        self.assertEqual(test_duration, self.repetition_base.repetitions[a - 1].time.value)
        self.assertEqual(test_show, self.repetition_base.repetitions[a - 1].show)

    def delete_repetition_entry(self):
        a = len(self.repetition_base.repetitions)
        self.repetition_base.delete_entry(self.repetition_base.repetitions[a-1].id.value)
        self.assertEqual(a-1, len(self.repetition_base.repetitions))

    def test_add_rent_entry(self):
        test_type = database_structure.RecvisiteType()
        test_type.id.value = 0
        test_type.type_name.value = "Unittest type"
        test_recvisite = database_structure.Recvisite()
        test_recvisite.id.value = 0
        test_recvisite.name.value = "Unittest recvisite"
        test_recvisite.price.value = 420.00
        test_recvisite.recv_type = test_type
        test_rent_date = datetime(2020,3,1)
        test_return_date = datetime(2020,3,2)
        test_show = database_structure.Show()
        test_show.id.value = 0
        test_show.name.value = "UnitTest Show"
        test_account = database_structure.Account()
        test_account.id.value = 0
        test_account.fio.value = "Unittest Account"
        test_account.login.value = 'U-Test Acc'
        test_account.password.value = sha512('test'.encode('utf-8')).hexdigest()
        test_account.address.value = 'UnittestCore'
        test_account.sex.value = 'женский'
        test_account.birthday.value = datetime(1998, 11, 23)
        test_show.account = test_account
        a = 0
        self.rent_base.add_entry(test_recvisite,test_rent_date,test_return_date,test_show,test_account)
        a = len(self.rent_base.rents)
        self.assertEqual(test_recvisite,self.rent_base.rents[a-1].recvisite)
        self.assertEqual(test_show, self.rent_base.rents[a - 1].show)
        self.assertEqual(test_recvisite, self.rent_base.rents[a - 1].recvisite)
        self.assertEqual(test_rent_date, self.rent_base.rents[a - 1].rent_date.value)
        self.assertEqual(test_return_date, self.rent_base.rents[a - 1].return_date.value)
        self.assertEqual(test_account, self.rent_base.rents[a - 1].recvisite_deputy)

    def delete_rent_entry(self):
        a = len(self.rent_base.rents)
        self.rent_base.delete_entry(self.rent_base.rents[a-1].id.value)
        self.assertEqual(a-1, len(self.rent_base.rents))

    def add_role_entry(self):
        test_show = database_structure.Show()
        test_show.id.value = 0
        test_show.name.value = "UnitTest Show"
        test_account = database_structure.Account()
        test_account.id.value = 0
        test_account.fio.value = "Unittest Account"
        test_account.login.value = 'U-Test Acc'
        test_account.password.value = sha512('test'.encode('utf-8')).hexdigest()
        test_account.address.value = 'UnittestCore'
        test_account.sex.value = 'женский'
        test_account.birthday.value = datetime(1998, 11, 23)
        test_show.account = test_account
        test_ampluah = database_structure.Ampluah()
        test_ampluah.id.value = 0
        test_ampluah.name.value = "Unittest Ampluah"
        test_ampluah_list = list()
        test_ampluah_list.append(test_ampluah)
        test_role = "Unittest role"
        a = 0
        self.role_base.add_entry(test_role,test_show,test_ampluah)
        a = len(self.role_base.roles)
        self.assertEqual(test_role,self.role_base.roles[a-1].name.value)
        self.assertEqual(test_show,self.role_base.roles[a-1].show)
        self.assertEqual(test_ampluah_list,self.role_base.roles[a-1].ampluah)

    def test_delete_role_entry(self):
        a = len(self.role_base.roles)
        self.role_base.delete_entry(self.role_base.roles[a-1].id.value)
        self.assertEqual(a-1, len(self.role_base.roles))

    def test_add_show_entry(self):
        test_account = database_structure.Account()
        test_account.id.value = 0
        test_account.fio.value = "Unittest Account"
        test_account.login.value = 'U-Test Acc'
        test_account.password.value = sha512('test'.encode('utf-8')).hexdigest()
        test_account.address.value = 'UnittestCore'
        test_account.sex.value = 'женский'
        test_account.birthday.value = datetime(1998, 11, 23)
        a = 0
        self.shows_base.add_entry("UnitTest Show", test_account)
        a = len(self.shows_base.shows)
        self.assertEqual("UnitTest Show",self.shows_base.shows[a-1].name.value)

    def test_delete_show_entry(self):
        a = len(self.shows_base.shows)
        self.shows_base.delete_entry(self.shows_base.shows[a-1].id.value)
        self.assertEqual(a-1,len(self.shows_base.shows))

    def test_add_tittle_entry(self):
        a = 0
        self.tittle_base.add_entry("UnitTest Tittle")
        a = len(self.tittle_base.tittles)
        self.assertEqual("UnitTest Tittle",self.tittle_base.tittles[a-1].name.value)

    def test_delete_tittle_entry(self):
        a = len(self.tittle_base.tittles)
        self.tittle_base.delete_entry(self.tittle_base.tittles[a-1].id.value)
        self.assertEqual(a-1,len(self.tittle_base.tittles))

    def test_add_type_entry(self):
        a = 0
        self.type_base.add_entry("UnitTest Type")
        a = len(self.type_base.types)
        self.assertEqual("UnitTest Type", self.type_base.types[a - 1].type_name.value)

    def test_delete_type_entry(self):
        a = len(self.type_base.types)
        self.tittle_base.delete_entry(self.type_base.types[a - 1].id.value)
        self.assertEqual(a, len(self.type_base.types))


if __name__ == '__main__':
    unittest.main()
