import database_structure, os, pickle

_AccountEnum = 0
_OccupationEnum = 0
_TittleEnum = 0
_ShowEnum = 0
_PosterEnum = 0
_RoleEnum = 0
_RepetitionEnum = 0
_RecvisiteEnum = 0
_RentEnum = 0
_AmpluahEnum = 0
_TypeEnum = 0


def _next_account():
    """Функция автоинкремента для аккаунтов"""
    global _AccountEnum
    _AccountEnum += 1
    return int(_AccountEnum)


def _next_occupation():
    """Функция автоинкремента для профессий"""
    global _OccupationEnum
    _OccupationEnum += 1
    return int(_OccupationEnum)


def _next_tittle():
    """Функция автоинкремента для званий"""
    global _TittleEnum
    _TittleEnum += 1
    return int(_TittleEnum)


def _next_repetition():
    """Функция автоинкремента для репетиций"""
    global _RepetitionEnum
    _RepetitionEnum += 1
    return int(_RepetitionEnum)


def _next_poster():
    """Функция автоинкремента для афиши"""
    global _PosterEnum
    _PosterEnum += 1
    return int(_PosterEnum)


def _next_recvisite():
    """Функция автоинкремента для реквизита"""
    global _RecvisiteEnum
    _RecvisiteEnum += 1
    return int(_RecvisiteEnum)


def _next_rent():
    """Функция автоинкремента для аренды"""
    global _RentEnum
    _RentEnum += 1
    return int(_RentEnum)


def _next_role():
    """Функция автоинкремента для ролей"""
    global _RoleEnum
    _RoleEnum += 1
    return int(_RoleEnum)


def _next_ampluah():
    """Функция автоинкремента для амплуа"""
    global _AmpluahEnum
    _AmpluahEnum += 1
    return int(_AmpluahEnum)


def _next_type():
    """Функция автоинкремента для типов"""
    global _TypeEnum
    _TypeEnum += 1
    return int(_TypeEnum)


def _next_show():
    """Функция автоинкремента спектаклей"""
    global _ShowEnum
    _ShowEnum+=1
    return int(_ShowEnum)


class AccountFactory:
    """Простая фабрика для генерации записей аккаунтов"""
    def __init__(self):
        """Инициализация фабрики"""
        self.accounts = list()
        if os.path.exists(os.path.join(os.getcwd(), 'pickle_dumps/accounts.pkl')):
            self.restore()
            global _AccountEnum
            _AccountEnum = self.accounts[len(self.accounts) - 1].id.value

    def dump(self):
        """Функция дампа списка записей"""
        with open(os.path.join(os.getcwd(), 'pickle_dumps/accounts.pkl'), 'wb') as file:
            pickle.dump(self.accounts, file)
            file.close()

    def restore(self):
        """Функция восстановления списка из дампа"""
        with open(os.path.join(os.getcwd(), 'pickle_dumps/accounts.pkl'), 'rb') as file:
            self.accounts = pickle.load(file)
            file.close()

    def watch_accounts(self):
        """Функция просмотра записей"""
        for account in self.accounts:
            print(account)

    def add_entry(self, fio, login, password, address, sex, birthday):
        """Функция добавления записи"""
        acc = database_structure.Account()
        acc.id.value = _next_account()
        acc.fio.value = fio
        acc.login.value = login
        acc.password.value = password
        acc.address.value = address
        acc.sex.value = sex
        acc.birthday.value = birthday
        self.accounts.append(acc)
        self.dump()

    def delete_entry(self, id):
        """Функция удаления записей"""
        for account in self.accounts:
            if id == account.id.value:
                self.accounts.pop(self.accounts.index(account))
        self.dump()


class AmpluahFactory:
    def __init__(self):
        self.ampluah_list = list()
        if os.path.exists(os.path.join(os.getcwd(), 'pickle_dumps/ampluah.pkl')):
            self.restore()
            global _AmpluahEnum
            _AmpluahEnum = self.ampluah_list[len(self.ampluah_list) - 1].id.value

    def dump(self):
        with open(os.path.join(os.getcwd(), 'pickle_dumps/ampluah.pkl'), 'wb') as file:
            pickle.dump(self.ampluah_list, file)
            file.close()

    def restore(self):
        with open(os.path.join(os.getcwd(), 'pickle_dumps/ampluah.pkl'), 'rb') as file:
            self.ampluah_list = pickle.load(file)
            file.close()

    def watch_ampluah(self):
        for ampluah in self.ampluah_list:
            print(ampluah)

    def add_entry(self, name):
        ampluah = database_structure.Ampluah()
        ampluah.id.value = _next_ampluah()
        ampluah.name.value = name
        self.ampluah_list.append(ampluah)
        self.dump()

    def delete_entry(self, id):
        for ampluah in self.ampluah_list:
            if id == ampluah.id.value:
                self.ampluah_list.pop(self.ampluah_list.index(ampluah))
        self.dump()


class OccupationFactory:
    def __init__(self):
        
        self.occupations = list()
        if os.path.exists(os.path.join(os.getcwd(), 'pickle_dumps/occupations.pkl')):
            self.restore()
            global _OccupationEnum
            _OccupationEnum = self.occupations[len(self.occupations) - 1].id.value

    def dump(self):
        with open(os.path.join(os.getcwd(), 'pickle_dumps/occupations.pkl'), 'wb') as file:
            pickle.dump(self.occupations, file)
            file.close()

    def restore(self):
        with open(os.path.join(os.getcwd(), 'pickle_dumps/occupations.pkl'), 'rb') as file:
            self.occupations = pickle.load(file)
            file.close()

    def watch_occupations(self):
        for occupation in self.occupations:
            print(occupation)

    def add_entry(self, occupation, tittles):
        occ = database_structure.Occupation()
        occ.id.value = _next_occupation()
        occ.name.value = occupation
        occ.tittles = tittles
        self.occupations.append(occ)
        self.dump()

    def delete_entry(self, id):
        for occupation in self.occupations:
            if id == occupation.id.value:
                self.occupations.pop(self.occupations.index(occupation))
        self.dump()


class PosterFactory:
    def __init__(self):
        self.posters = list()
        if os.path.exists(os.path.join(os.getcwd(), 'pickle_dumps/posters.pkl')):
            self.restore()
            global _PosterEnum
            _PosterEnum = self.posters[len(self.posters) - 1].id.value

    def dump(self):
        with open(os.path.join(os.getcwd(), 'pickle_dumps/posters.pkl'), 'wb') as file:
            pickle.dump(self.posters, file)
            file.close()

    def restore(self):
        with open(os.path.join(os.getcwd(), 'pickle_dumps/posters.pkl'), 'rb') as file:
            self.posters = pickle.load(file)
            file.close()

    def watch_posters(self):
        for poster in self.posters:
            print(poster)

    def add_entry(self, show, date, duration):
        poster = database_structure.Poster()
        poster.id.value = _next_poster()
        poster.show = show
        poster.date.value = date
        poster.duration.value = duration
        self.posters.append(poster)
        self.dump()

    def delete_entry(self, id):
        for poster in self.posters:
            if id == poster.id.value:
                self.posters.pop(self.posters.index(poster))
        self.dump()


class RecvisiteFactory:
    def __init__(self):
        self.recvisite_list = list()
        if os.path.exists(os.path.join(os.getcwd(), 'pickle_dumps/recvisite_list.pkl')):
            self.restore()
            global _RecvisiteEnum
            _RecvisiteEnum = self.recvisite_list[len(self.recvisite_list) - 1].id.value

    def dump(self):
        with open(os.path.join(os.getcwd(), 'pickle_dumps/recvisite_list.pkl'), 'wb') as file:
            pickle.dump(self.recvisite_list, file)
            file.close()

    def restore(self):
        with open(os.path.join(os.getcwd(), 'pickle_dumps/recvisite_list.pkl'), 'rb') as file:
            self.recvisite_list = pickle.load(file)
            file.close()

    def watch_recvisite_list(self):
        for recvisite in self.recvisite_list:
            print(recvisite)

    def add_entry(self, name, price, recv_type):
        recvisite = database_structure.Recvisite()
        recvisite.id.value = _next_recvisite()
        recvisite.name.value = name
        recvisite.price.value = price
        recvisite.recv_type = recv_type
        self.recvisite_list.append(recvisite)
        self.dump()

    def delete_entry(self, id):
        for recvisite in self.recvisite_list:
            if id == recvisite.id.value:
                self.recvisite_list.pop(self.recvisite_list.index(recvisite))
        self.dump()


class RepetitionFactory:
    def __init__(self):
        self.repetitions = list()
        if os.path.exists(os.path.join(os.getcwd(), 'pickle_dumps/repetitions.pkl')):
            self.restore()
            global _RepetitionEnum
            _RepetitionEnum = self.repetitions[len(self.repetitions) - 1].id.value

    def dump(self):
        with open(os.path.join(os.getcwd(), 'pickle_dumps/repetitions.pkl'), 'wb') as file:
            pickle.dump(self.repetitions, file)
            file.close()

    def restore(self):
        with open(os.path.join(os.getcwd(), 'pickle_dumps/repetitions.pkl'), 'rb') as file:
            self.repetitions = pickle.load(file)
            file.close()

    def watch_repetitions(self):
        for repetition in self.repetitions:
            print(repetition)

    def add_entry(self, date, time, show):
        repetition = database_structure.Repetition()
        repetition.id.value = _next_repetition()
        repetition.date.value = date
        repetition.time.value = time
        repetition.show = show
        self.repetitions.append(repetition)
        self.dump()

    def delete_entry(self, id):
        for repetition in self.repetitions:
            if id == repetition.id.value:
                self.repetitions.pop(self.repetitions.index(repetition))
        self.dump()


class RentFactory:
    def __init__(self):
        self.rents = list()
        if os.path.exists(os.path.join(os.getcwd(), 'pickle_dumps/rents.pkl')):
            self.restore()
            global _RentEnum
            _RentEnum = self.rents[len(self.rents) - 1].id.value

    def dump(self):
        
        with open(os.path.join(os.getcwd(), 'pickle_dumps/rents.pkl'), 'wb') as file:
            pickle.dump(self.rents, file)
            file.close()

    def restore(self):
        
        with open(os.path.join(os.getcwd(), 'pickle_dumps/rents.pkl'), 'rb') as file:
            self.rents = pickle.load(file)
            file.close()

    def watch_rents(self):
        
        for rent in self.rents:
            print(rent)

    def add_entry(self, recvisite, rent_date, return_date, show, account):
        rent = database_structure.Rent()
        rent.id.value = _next_rent()
        rent.recvisite = recvisite
        rent.rent_date.value = rent_date
        rent.return_date.value = return_date
        rent.show = show
        rent.recvisite_deputy = account
        self.rents.append(rent)
        self.dump()

    def delete_entry(self, id):
        for rent in self.rents:
            if id == rent.id.value:
                self.rents.pop(self.rents.index(rent))
        self.dump()


class RoleFactory:
    def __init__(self):
        self.roles = list()
        if os.path.exists(os.path.join(os.getcwd(), 'pickle_dumps/roles.pkl')):
            self.restore()
            global _RoleEnum
            _RoleEnum = self.roles[len(self.roles) - 1].id.value

    def dump(self):

        with open(os.path.join(os.getcwd(), 'pickle_dumps/roles.pkl'), 'wb') as file:
            pickle.dump(self.roles, file)
            file.close()

    def restore(self):

        with open(os.path.join(os.getcwd(), 'pickle_dumps/roles.pkl'), 'rb') as file:
            self.roles = pickle.load(file)
            file.close()

    def watch_roles(self):

        for role in self.roles:
            print(role)

    def add_entry(self, name, show, ampluah):
        role = database_structure.Role()
        role.id.value = _next_role()
        role.name.value = name
        role.show = show
        role.ampluah = ampluah
        self.roles.append(role)
        self.dump()

    def delete_entry(self, id):

        for role in self.roles:
            if id == role.id.value:
                self.roles.pop(self.roles.index(role))
        self.dump()


class ShowsFactory:
    
    def __init__(self):
        
        self.shows = list()
        if os.path.exists(os.path.join(os.getcwd(), 'pickle_dumps/shows.pkl')):
            self.restore()
            global _ShowEnum
            _ShowEnum = self.shows[len(self.shows) - 1].id.value

    def dump(self):
        
        with open(os.path.join(os.getcwd(), 'pickle_dumps/shows.pkl'), 'wb') as file:
            pickle.dump(self.shows, file)
            file.close()

    def restore(self):
        
        with open(os.path.join(os.getcwd(), 'pickle_dumps/shows.pkl'), 'rb') as file:
            self.shows = pickle.load(file)
            file.close()

    def watch_shows(self):
        
        for show in self.shows:
            print(show)

    def add_entry(self, name, account):
        show = database_structure.Show()
        show.id.value = _next_show()
        show.name.value = name
        show.account = account
        self.shows.append(show)
        self.dump()

    def delete_entry(self, id):
        
        for show in self.shows:
            if id == show.id.value:
                self.shows.pop(self.shows.index(show))
        self.dump()


class TittleFactory:
    
    def __init__(self):
        
        self.tittles = list()
        if os.path.exists(os.path.join(os.getcwd(), 'pickle_dumps/tittles.pkl')):
            self.restore()
            global _TittleEnum
            _TittleEnum = self.tittles[len(self.tittles) - 1].id.value

    def dump(self):
        
        with open(os.path.join(os.getcwd(), 'pickle_dumps/tittles.pkl'), 'wb') as file:
            pickle.dump(self.tittles, file)
            file.close()

    def restore(self):
        
        with open(os.path.join(os.getcwd(), 'pickle_dumps/tittles.pkl'), 'rb') as file:
            self.tittles = pickle.load(file)
            file.close()

    def watch_tittles(self):
        for tittle in self.tittles:
            print(tittle)

    def add_entry(self, name):
        tittle = database_structure.Tittle()
        tittle.id.value = _next_tittle()
        tittle.name.value = name
        self.tittles.append(tittle)
        self.dump()

    def delete_entry(self, id):
        for tittle in self.tittles:
            if id == tittle.id.value:
                self.tittles.pop(self.tittles.index(tittle))
        self.dump()


class TypeFactory:
    def __init__(self):
        self.types = list()
        if os.path.exists(os.path.join(os.getcwd(), 'pickle_dumps/types.pkl')):
            self.restore()
            global _TypeEnum
            _TypeEnum = self.types[len(self.types) - 1].id.value

    def dump(self):
        with open(os.path.join(os.getcwd(), 'pickle_dumps/types.pkl'), 'wb') as file:
            pickle.dump(self.types, file)
            file.close()

    def restore(self):
        with open(os.path.join(os.getcwd(), 'pickle_dumps/types.pkl'), 'rb') as file:
            self.types = pickle.load(file)
            file.close()

    def watch_types(self):
        for type in self.types:
            print(type)

    def add_entry(self, name):
        type = database_structure.RecvisiteType()
        type.id.value = _next_type()
        type.type_name.value = name
        self.types.append(type)
        self.dump()

    def delete_entry(self, id):
        for type in self.types:
            if id == type.id.value:
                self.types.pop(self.types.index(type))
        self.dump()

