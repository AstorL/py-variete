import hashlib
from datetime import *


class AutoProperty(object):
    """Класс имитирующий автоматические свойства из языка C#, вида (public "тип переменной" value { get; set; })"""
    def __init__(self, prop_type=None, message="Данная переменная не несет в себе значение\nПерегрузите констркутор"):
        """Инициализация автосвойства"""
        self.__value = None
        self.__prop_type = prop_type
        self.__message = str(message)

    @property
    def value(self):
        """получение значения автосвойства"""
        return self.__value

    @value.setter
    def value(self, value):
        """Задание значения автосвойства"""
        if not isinstance(value, self.__prop_type):
            raise TypeError("{0}, тип введенных данных {1}".format(self.__message, type(value)))
        else:
            self.__value = value


class Account(object):
    """Структура учетной записи"""
    def __init__(self):
        """Метод инициализации"""
        self.id = AutoProperty(int, "ID должен быть целочисленным")
        self.fio = AutoProperty(str, "ФИО должно быть строковой переменной")
        self.login = AutoProperty(str, "Логин должен быть строковым")
        self.password = AutoProperty(str, "Пароль является строковым значением")
        self.address = AutoProperty(str, "Пароль является строковой переменной")
        self.sex = AutoProperty(str, "Пол является строковой величиной")
        self.birthday = AutoProperty(datetime, "Поле должно являться датой")
        self.registration_date = datetime.now()
        self.occupation = None
        self.tittles = []
        self.account_roles = []
        
           

    def __str__(self):
        """Переопределенный метод строкового отображения"""
        string = ""
        string += "ID: {0}\nФИО: {1}\nЛогин: {2}\nПароль: {3}\n".\
            format(self.id.value, self.fio.value, self.login.value, self.password.value)
        string += "Адрес: {0}\nПол: {1}\nДата рождения: {2}\n".format(self.address.value, self.sex.value,
                                                                      self.birthday.value)
        string += "Дата регистрации: {0}\n".format(self.registration_date)
        if self.occupation is not None and len(self.tittles) != 0:
            string += "Профессия: {0}\n".format(self.occupation.name.value)
            string += "Звания:\n"
            for tittle in self.tittles:
                string += "{0}\n".format(tittle.name.value)
        return string


class Ampluah(object):
    """Структура записи амплуа"""
    def __init__(self):
        """Метод инициализации"""
        self.id = AutoProperty(int, "ID должен быть целочисленным")
        self.name = AutoProperty(str, "Название должно быть строкой")
        
           

    def __str__(self):
        """Переопределнный метод строкового отображения"""
        return "ID: {0}\nНазвание: {1}\n".format(self.id.value, self.name.value)


class Occupation(object):
    """Структура записи профессии"""
    def __init__(self):
        """Метод инициализации"""
        self.id = AutoProperty(int, "ID должен быть целочисленным")
        self.name = AutoProperty(str, "Имя должно быть строкой")
        self.registration_date = datetime.now()
        self.tittles = list()
        
           

    def __str__(self):
        """Переопределенный метод строкового отображения"""
        string = str("")
        string += "ID: {0}\nНазвание: {1}\nДата регистрации: {2}\n".format(self.id.value, self.name.value,
                                                                           self.registration_date)
        string += "Звания:\n"
        for tittle in self.tittles:
            string += "ID: {0}\nЗвание: {1}\n".format(tittle.id.value, tittle.name.value)
        return string


class Poster(object):
    """Структура записи афиши"""
    def __init__(self):
        """Метод инициализации"""
        self.id = AutoProperty(int, "ID должен быть целочисленным")
        self.show = None
        self.date = AutoProperty(datetime, "Данный тип не является датой")
        self.duration = AutoProperty(time, "Для записи времени, используйте соответсвующий класс")

    def __str__(self):
        """Переопределенный метод строкового отображения"""
        return "ID: {0}\nСпетакль: {1}\nДата: {2}\nПродолжительность {3}\nРежиссер {4}\n" \
            .format(self.id.value, self.show.value.name, self.date.value, self.duration.value,
                    self.show.value.account.value.fio.value)


class Recvisite(object):
    """Структура записи реквизита"""
    def __init__(self):
        """Метод ининциализации"""
        self.id = AutoProperty(int, "ID должен быть целочисленным")
        self.name = AutoProperty(str, "Имя не является строкой")
        self.price = AutoProperty(float, "Цена является числом с плавающей точкой")
        self.recv_type = None
        

    def __str__(self):
        """Переопределенный метод строкового преображения"""
        string = str("")
        if self.recv_type is not None:
            string = "ID: {0}\nНазвание: {1}\nЦена: {2}\nТип {3}\n" \
                .format(self.id.value, self.name.value, self.price.value, self.recv_type.type_name.value)
        return string


class Repetition(object):
    """Стркутура записи репетиции"""
    def __init__(self):
        """Метод инициализации"""
        self.id = AutoProperty(int, "ID должен быть целочисленным")
        self.date = AutoProperty(datetime, "Данный тип не является датой")
        self.time = AutoProperty(time, "Данный тип не является ")
        self.show = None
        
           

    def __str__(self):
        """Переопределенный метод строкового преобразования"""
        string = str("")
        if self.show is not None:
            string += "ID: {0}\nДата: {1}\nВремя: {2}\nСпектакль {3}" \
                .format(self.id.value, self.date.value, self.time.value, self.show.name.value)
        return string


class Rent(object):
    """Структура записи аренды"""
    def __init__(self):
        """Метод инициализации"""
        self.id = AutoProperty(int, "ID должен быть целочисленным")
        self.rent_date = AutoProperty(datetime, "Данный тип не является датой")
        self.return_date = AutoProperty(datetime, "Данный тип не является датой")
        self.recvisite = None
        self.show = None
        self.recvisite_deputy = None


    def __str__(self):
        """Переопределенный метод строкового отображения"""
        string = str("")
        if self.recvisite is not None and self.show is not None and self.recvisite_deputy is not None:
            string += "ID: {0}\nРеквизит: {1}\nЦена: {2}\nСпектакль: {3}\n".format(self.id.value, self.recvisite.name.value, self.recvisite.price.value,
                        self.show.name.value)
            string += "Выдал: {0}\nДата выдачи {1}\nДата возврата {2}\n".format(self.recvisite_deputy.fio.value,self.rent_date.value, self.return_date.value)
        return string


class Role(object):
    """Структура записи роли"""
    def __init__(self):
        """Метод инициализации"""
        self.id = AutoProperty(int, "ID должен быть целочисленным")
        self.show = None
        self.name = AutoProperty(str, "Имя не является строкой")
        self.ampluah = list()

    def __str__(self):
        """Переопределенный метод строкового отображения"""
        string = str()
        string += "ID: {0}\nСпектакль: {1}\n Название роли: {2}\n".format(self.id.value, self.show.name.value,
                                                                        self.name.value)
        string += "Амплуа:\n"
        for ampluah in self.ampluah:
            string += ampluah.name+"\n"
        return string


class RecvisiteType(object):
    """Структура записи типа реквизита"""
    def __init__(self):
        """Метод инициализации"""
        self.id = AutoProperty(int, "ID должен быть целочисленным")
        self.type_name = AutoProperty(str, "Имя типа не является строкой")

    def __str__(self):
        """Переопределнный метод строкового отображения"""
        return "ID: {0}\nТип: {1}\n".format(self.id.value, self.type_name.value)


class Show(object):
    """Структура записи спектакля"""
    def __init__(self):
        """Метод инициализации"""
        self.id = AutoProperty(int, "ID должен быть целочисленным")
        self.name = AutoProperty(str, "Имя не является строкой")
        self.account = None

    def __str__(self):
        """Переопределнный метод строкового отображения"""
        return "ID: {0}\nСпектакль: {1}\n: {2}\n" \
            .format(self.id.value, self.name.value, self.account.fio.value)


class Tittle(object):
    """Структура записи звания"""
    def __init__(self):
        """Метод инициализации"""
        self.id = AutoProperty(int, "ID должен быть целочисленным")
        self.name = AutoProperty(str, "Звание должно быть строкой")
        self.registration_date = datetime.now()

    def __str__(self):
        """Переопреденный метод строкового отображения"""
        return "ID: {0}\nЗвание: {1}\nДата регистрации: {2}\n".format(self.id.value, self.name.value, self.registration_date)
