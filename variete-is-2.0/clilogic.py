from datetime import datetime, time
from hashlib import sha512
import os, database

_AuthorizedAccountIndex = None

acc_base = database.AccountFactory()
ampluah_base = database.AmpluahFactory()
occ_base = database.OccupationFactory()
posters_base = database.PosterFactory()
recvisite_base = database.RecvisiteFactory()
repetition_base = database.RepetitionFactory()
rent_base = database.RentFactory()
role_base = database.RoleFactory()
shows_base = database.ShowsFactory()
tittle_base = database.TittleFactory()
type_base = database.TypeFactory()


def startup():
    input_choice = int(0)
    input_choices = {
        1: lambda: authorize(),
        2: lambda: register()
    }

    while (input_choice != 3):
        print('\n'
              '1. Авторизация.\n'
              '2. Регистрация.\n'
              '3. Выход.')
        try:
            input_choice = (int(input("Выберите действие: ")))
        except:
            print('Произошла ошибка, введите комманду еще раз')
        if input_choice in input_choices:
            input_choices[input_choice]()


def authorize():
    global _AuthorizedAccountIndex
    print("Авторизация")
    login = input("Введите логин: ")
    password = sha512(input("Введите пароль: ").encode('utf-8')).hexdigest()
    for account in acc_base.accounts:
        if account.login.value == login and account.password.value == password:
            _AuthorizedAccountIndex = acc_base.accounts.index(account)
            main_menu()


def register():
    accounts_sex = {
        1: "Мужской",
        2: "Женский"
    }
    print("Регистрация")
    fio = input("Введите ФИО: ")
    login = input("Введите логин: ")
    password = sha512(input("Введите пароль: ").encode('utf-8')).hexdigest()
    address = input("Введите адрес: ")
    sex = accounts_sex[int(input("Введите пол (1 - мужской, 2 - женский): "))]
    birthday = input("Введите дату рождения в формате (ГГГГ-ММ-ДД): ")
    try:
        acc_base.add_entry(fio, login, password, address, sex,
                       datetime(int(birthday.split('-')[0]), int(birthday.split('-')[1]), int(birthday.split('-')[2])))
    except TypeError as err:
        print(err)


def main_menu():
    print("Главное меню")
    input_choice = int(0)
    input_choices = {
        1: lambda: accounts_work(),
        2: lambda: actors_work(),
        3: lambda: producer_work(),
        4: lambda: recvisite_work()
    }
    while (input_choice != 5):
        print('\n'
              '1. Работа с аккаунтами.\n'
              '2. Работа с актерами.\n'
              '3. Работа с режиссерами.\n'
              '4. Работа с реквизитом.\n'
              '5. Назад.')
        try:
            input_choice = (int(input("Выберите действие: ")))
        except:
            print('Произошла ошибка, введите комманду еще раз')
        if input_choice in input_choices:
            input_choices[input_choice]()


def accounts_work():
    print("Работа с учетными запиями")
    input_choice = int(0)
    input_choices = {
        1: lambda: add_extra_info(),
        2: lambda: acc_base.watch_accounts(),
        3: lambda: posters_base.watch_posters(),
        4: lambda: delete_account(),
        5: lambda: occupations_work(),
        6: lambda: tittles_work()
    }
    while (input_choice != 7):
        print('\n'
              '1. Добавить подробную информацию.\n'
              '2. Просмотр аккаунтов.\n'
              '3. Просмотр афиши.\n'
              '4. Удалить аккаунт.\n'
              '5. Действия над профессиями.\n'
              '6. Действия над званиями.\n'
              '7. Назад')
        try:
            input_choice = (int(input("Выберите действие: ")))
        except:
            print('Произошла ошибка, введите комманду еще раз')
        if input_choice in input_choices:
            input_choices[input_choice]()


def add_extra_info():
    global _AuthorizedAccountIndex
    acc_tittles = list()
    inp = 0
    print("Заполнение дополнительной информации об учетной записи\n")
    print("Список профессий")
    occ_base.watch_occupations()
    try:
        occ_id = int(input("Введите ID профессии: "))
    except:
        print('Произошла ошибка, введите комманду еще раз')
    for occupation in occ_base.occupations:
        if occ_id == occupation.id.value:
            acc_base.accounts[_AuthorizedAccountIndex].occupation = occupation
    while inp != "выход":
        inp = input("Введите ID звания или выход для завершения ввода: ")
        if inp != "выход":
            for tittle in tittle_base.tittles:
                if int(inp) == tittle.id.value:
                    acc_tittles.append(tittle)
    acc_base.accounts[_AuthorizedAccountIndex].tittles = acc_tittles
    acc_base.dump()


def delete_account():
    print("Удаление аккаунтов")
    acc_base.watch_accounts()
    try:
        acc_base.delete_entry(int(input("Введите ID удаляемой записи: ")))
    except:
        print('Произошла ошибка, введите комманду еще раз')


def occupations_work():
    print("Действия над профессиями")
    input_choice = int(0)
    input_choices = {
        1: lambda: occ_base.watch_occupations(),
        2: lambda: add_occupation(),
        3: lambda: delete_occupation()
    }
    while (input_choice != 4):
        print('\n'
              '1. Просмотр профессий.\n'
              '2. Добавить профессию.\n'
              '3. Удалить профессию.\n'
              '4. Назад')
        try:
            input_choice = (int(input("Выберите действие: ")))
        except:
            print('Произошла ошибка, введите комманду еще раз')
        if input_choice in input_choices:
            input_choices[input_choice]()


def add_occupation():
    print("Добавление профессии")
    tittle_base.watch_tittles()
    occ = input("Введите профессиию: ")
    inp = 0
    ttl_list = []
    while inp != "выход":
        inp = input("Введите ID звания или выход для завершения ввода: ")
        if inp != "выход":
            for tittle in tittle_base.tittles:
                if int(inp) == tittle.id.value:
                    ttl_list.append(tittle)
                    print(tittle.name.value)
    try:
        occ_base.add_entry(occ, ttl_list)
    except TypeError as err:
        print(err)


def delete_occupation():
    print("Удаление профессии")
    occ_base.watch_occupations()
    try:
        occ_base.delete_entry(int(input("Введите ID удаляемой записи: ")))
    except:
        print('Произошла ошибка, введите комманду еще раз')


def tittles_work():
    print("Действия над званиями")
    input_choice = int(0)
    input_choices = {
        1: lambda: tittle_base.watch_tittles(),
        2: lambda: add_tittle(),
        3: lambda: delete_tittle()
    }
    while (input_choice != 4):
        print('\n'
              '1. Просмотр званий.\n'
              '2. Добавить звание.\n'
              '3. Удалить звания.\n'
              '4. Назад')
        try:
            input_choice = (int(input("Выберите действие: ")))
        except:
            print('Произошла ошибка, введите комманду еще раз')
        if input_choice in input_choices:
            input_choices[input_choice]()


def add_tittle():
    print('Добавление званий')
    occ_base.watch_occupations()
    try:
        tittle_base.add_entry(input("Введите звание: "))
    except:
        print('Произошла ошибка, введите комманду еще раз')


def delete_tittle():
    print("Удаление звания")
    tittle_base.watch_tittles()
    try:
        tittle_base.delete_entry(int(input("Введите ID удаляемой записи: ")))
    except:
        print('Произошла ошибка, введите комманду еще раз')


def actors_work():
    print("Действия актеров")
    input_choice = int(0)
    input_choices = {
        1: lambda: repetition_base.watch_repetitions(),
        2: lambda: watch_roles()
    }
    while (input_choice != 3):
        print('\n'
              '1. Просмотр репетиций.\n'
              '2. Просмотр ролей.\n'
              '3. Назад.')
        try:
            input_choice = (int(input("Выберите действие: ")))
        except:
            print('Произошла ошибка, введите комманду еще раз')
        if input_choice in input_choices:
            input_choices[input_choice]()


def watch_roles():
    global _AuthorizedAccountIndex
    for role in acc_base.accounts[_AuthorizedAccountIndex].roles:
        print(role)


def producer_work():
    print("Действия режиссеров")
    input_choice = int(0)
    input_choices = {
        1: lambda: repetition_base.watch_repetitions(),
        2: lambda: role_base.watch_roles(),
        3: lambda: ampluah_base.watch_ampluah(),
        4: lambda: shows_base.watch_shows(),
        5: lambda: add_show(),
        6: lambda: add_repetition(),
        7: lambda: add_poster(),
        8: lambda: add_ampluah(),
        9: lambda: add_role(),
        10: lambda: assign_role(),
        11: lambda: delete_ampluah(),
        12: lambda: delete_role(),
        13: lambda: delete_show(),
        14: lambda: delete_repetition(),
        15: lambda: delete_poster()
    }
    while input_choice != 16:
        print('\n'
              '1. Просмотр репетиций.\n'
              '2. Просмотр ролей.\n'
              '3. Просмотр амплуа.\n'
              '4. Просмотр спектаклей.\n'
              '5. Добавить спектакль.\n'
              '6. Добавить репетицию.\n'
              '7. Добавить афишу.\n'
              '8. Добавить амплуа.\n'
              '9. Добавить роль.\n'
              '10. Назначить роль.\n'
              '11. Удалить амплуа.\n'
              '12. Удалить роль.\n'
              '13. Удалить спекаткль.\n'
              '14. Удалить репетицию.\n'
              '15. Удалить афишу.\n'
              '16. Назад.')
        try:
            input_choice = (int(input("Выберите действие: ")))
        except:
            print('Произошла ошибка, введите комманду еще раз')
        if input_choice in input_choices:
            input_choices[input_choice]()


def add_show():
    global _AuthorizedAccountIndex
    print("Добавление спектакля")
    shows_base.add_entry(input("Введите название спектакля: "), acc_base.accounts[_AuthorizedAccountIndex])


def add_repetition():
    print("Добавление репетеции")
    print("Список спектаклей")
    shows_base.watch_shows()
    show_id = input("Введите ID спектакля: ")
    rep_date = input("Введите дату репетиции: ")
    rep_time = input("Введите время репетиции: ")
    rep_show = None
    for show in shows_base.shows:
        if show_id == show.id.value:
            rep_show = show
    try:
        repetition_base.add_entry(
            datetime(int(rep_date.split(" ")[0].split("-")[0]), int(rep_date.split(" ")[0].split("-")[1]),
                     int(rep_date.split(" ")[0].split("-")[2]),
                     int(rep_date.split(" ")[1].split(":")[0]), int(rep_date.split(" ")[1].split(":")[1])),
            time(int(rep_time.split(":")[0]), int(rep_time.split(":")[1])), rep_show)
    except TypeError as err:
        print(err)


def add_poster():
    print("Добавление афиши")
    print("Список спектаклей")
    shows_base.watch_shows()
    show_id = input("Введите ID спектакля: ")
    show_date = input("Введите дату выступления: ")
    show_time = input("Введите продолжительность выступления: ")
    poster_show = None
    for show in shows_base.shows:
        if show_id == show.id.value:
            poster_show = show
    try:
        posters_base.add_entry(poster_show, datetime(int(show_date.split(" ")[0].split("-")[0]), int(show_date.split(" ")[0].split("-")[1]),
                    int(show_date.split(" ")[0].split("-")[2]), int(show_date.split(" ")[1].split(":")[0])),
                    time(int(show_time.split(":")[0]), int(show_time.split(":")[1])))
    except TypeError as err:
        print(err)


def add_ampluah():
    print("Добавление амплуа")
    ampluah_name = input("Введите назавание амплуа: ")
    ampluah_base.add_entry(ampluah_name)


def add_role():
    print("Добавление роли")
    print("Список амплуа")
    ampluah_base.watch_ampluah()
    print("Список спектаклей")
    shows_base.watch_shows()
    role_name = input("Введите имя роли ")
    role_show_id = input("Введите ID спектакля ")
    role_show = None
    ampluah_inp = 0
    for show in shows_base.shows:
        if int(role_show_id) == show.id.value:
            role_show = show
    role_ampluah_list = list()
    while ampluah_inp != "выход":
        ampluah_inp = input("Введите ID амплуа или выход для завершения вывода ")
        for ampluah in ampluah_base.ampluah_list:
            if ampluah_inp != "выход":
                if int(ampluah_inp) == ampluah.id.value:
                    role_ampluah_list.append(ampluah)
    role_base.add_entry(role_name, role_show, role_ampluah_list)


def assign_role():
    print("Назначение роли")
    print("Список актеров")
    acc_base.watch_accounts()
    print("Список ролей")
    role_base.watch_roles()
    actor_id = input("Введите id актера ")
    role_id = 0
    actor_acc = None
    role_list = list()
    for account in acc_base.accounts:
        if actor_id == account.id.value:
            actor_acc = account
    while role_id != "выход":
        role_id = input("Введите ID роли или выход для завершения ввода: ")
        if role_id != "выход":
            for role in role_base.roles:
                if int(role_id) == role.id.value:
                    role_list.append(role)
    actor_acc.roles = role_list
    acc_base.dump()


def delete_ampluah():
    print("Удаление амплуа")
    ampluah_base.watch_ampluah()
    ampluah_base.delete_entry(int(input("Введите ID удаляемой записи ")))


def delete_role():
    print("Удаление роли")
    role_base.watch_roles()
    role_base.delete_entry(int(input("Введите ID удаляемой записи ")))


def delete_show():
    print("Удаление спектакля")
    shows_base.watch_shows()
    shows_base.delete_entry(int(input("Введите ID удаляемой записи ")))


def delete_repetition():
    print("Удаление репетиций")
    repetition_base.watch_repetitions()
    repetition_base.delete_entry(int(input("Введите ID удаляемой записи ")))


def delete_poster():
    print("Удаление афиши")
    posters_base.watch_posters()
    posters_base.delete_entry(int(input("Введите ID удаляемой записи")))


def recvisite_work():
    print("Действия реквизиторов")
    input_choice = int(0)
    input_choices = {
        1: lambda: recvisite_base.watch_recvisite_list(),
        2: lambda: type_base.watch_types(),
        3: lambda: add_recvisite(),
        4: lambda: add_type(),
        5: lambda: rent_recvisite(),
        6: lambda: rent_base.watch_rents(),
        7: lambda: rent_delete(),
        8: lambda: recvisite_delete(),
        9: lambda: type_delete()
    }
    while input_choice != 10:
        print('\n'
              '1. Просмотр реквизита.\n'
              '2. Просмотр типов.\n'
              '3. Добавление реквизита.\n'
              '4. Добавление типа.\n'
              '5. Выдача реквизита.\n'
              '6. Просмотр аренды.\n'
              '7. Закрытие аренды.\n'
              '8. Удаление реквизита.\n'
              '9. Удаление типа реквизита.\n'
              '10. Назад.')
        try:
            input_choice = (int(input("Выберите действие: ")))
        except:
            print('Произошла ошибка, введите комманду еще раз')
        if input_choice in input_choices:
            input_choices[input_choice]()


def add_recvisite():
    print("Добавление реквизита")
    recvisite_name = input("Введите название реквизита: ")
    recvisite_price = float(input("Введите цену реквизита: "))
    print("Типы реквизита")
    type_base.watch_types()
    type_id = int(input("Введите ID типа реквизита: "))
    recv_type = None
    for rec_type in type_base.types:
        if type_id == rec_type.id.value:
            recv_type = rec_type
    recvisite_base.add_entry(recvisite_name, recvisite_price, recv_type)


def add_type():
    print("Добавление типа")
    type_base.add_entry(input("Введите название типа: "))


def rent_recvisite():
    print("Выдача реквизита")
    global _AuthorizedAccountIndex
    recvisite_base.watch_recvisite_list()
    recvisite_id = int(input("Введите id арендуемого реквизита: "))
    return_date = input("Введите дату возврата: ")
    shows_base.watch_shows()
    show_id = int(input("Введите id спектакля: "))
    rent_recv = None
    rent_show = None
    for recvisite in recvisite_base.recvisite_list:
        if recvisite_id == recvisite.id.value:
            rent_recv = recvisite
    for show in shows_base.shows:
        if show_id == show.id.value:
            rent_show = show
    rent_base.add_entry(rent_recv,datetime.now(),
                        datetime(int(return_date.split("-")[0]), int(return_date.split("-")[1]),
                                 int(return_date.split("-")[2])), rent_show,acc_base.accounts[_AuthorizedAccountIndex])


def rent_delete():
    print("Закрытие аренды")
    rent_base.watch_rents()
    rent_base.delete_entry(int(input("Введите ID удаляемой записи: ")))


def recvisite_delete():
    print("Удаление реквизита")
    recvisite_base.watch_recvisite_list()
    recvisite_base.delete_entry(int(input("Введите ID удаляемой записи: ")))


def type_delete():
    print("Удаление типа")
    type_base.watch_types()
    type_base.delete_entry(int(input("Введите ID удаляемой записи: ")))


if __name__ == '__main__':
    if not os.path.exists(os.path.join(os.getcwd(), "pickle_dumps")):
        os.mkdir(os.path.join(os.getcwd(), "pickle_dumps"))
    startup()
